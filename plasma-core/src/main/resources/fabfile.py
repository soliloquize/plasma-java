# -*- encoding:utf-8 -*-

from __future__ import with_statement
from fabric.api import run, cd, hosts, execute
import os


def get_remote_path():
    with open('bin/config.properties') as inputs:
        for line in inputs:
            line = line.strip()
            if line.startswith('remote_path'):
                return line.split('=')[1].strip()
    return None


LOCAL_PATH = os.path.dirname(__file__)
REMOTE_PATH = get_remote_path()


@hosts('vultr')
def deploy():
    with cd(REMOTE_PATH):
        run('git pull')


@hosts('localhost')
def build():
    with cd(os.path.join(LOCAL_PATH, 'public')):
        run('rm -r *')
    with cd(LOCAL_PATH):
        run('java -jar bin/plasma-core-2.0-SNAPSHOT-jar-with-dependencies.jar -c bin/config.properties -m build')


@hosts('localhost')
def commit():
    with cd(LOCAL_PATH):
        run('git add .')
        run('git commit -am "add content"')
        run('git push -u origin master')


@hosts('localhost')
def new(title):
    with cd(LOCAL_PATH):
        run('java -jar bin/plasma-core-2.0-SNAPSHOT-jar-with-dependencies.jar -c bin/config.properties -m create "%s"' % title)


def publish():
    execute(build)
    execute(commit)
    execute(deploy)
