package org.soliloquize.plasma.generator.impl;

import org.soliloquize.plasma.core.Config;
import org.soliloquize.plasma.generator.NavManager;
import org.soliloquize.plasma.model.Article;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArticleGenerator extends HtmlGenerator {

    private NavManager navManager = new NavManager();

    private boolean useNavManager;

    public ArticleGenerator(boolean useNavManager) {
        this.useNavManager = useNavManager;
    }

    @Override
    protected String getTemplateName() {
        return "article.html";
    }

    @Override
    public void generate(List<Article> articleList) throws IOException {
        String nav = navManager.isValid() && useNavManager ? navManager.generate() : null;
        for (Article article : articleList) {
            Map<String, Object> context = new HashMap<>();
            context.put("article", article);
            context.put("nav", nav);
            save(getOutputFile(article.getMeta().getSlug()), render(context));
        }
        if (useNavManager && navManager.isValid()) {
            copyLastArticle(articleList);
        }
    }

    private void copyLastArticle(List<Article> articleList) throws IOException {
        articleList.sort((x, y) -> y.getMeta().getCreatedTime().compareTo(x.getMeta().getCreatedTime()));
        Article article = articleList.get(0);
        String dstFolder = Config.getInstance().getDstFolder();
        String path = article.getMeta().getSlug().substring(1);
        File last = new File(new File(dstFolder, path).getAbsolutePath(), "index.html");
        File dstFile = new File(dstFolder, "index.html");
        if (dstFile.exists()) {
            dstFile.delete();
        }
        Files.copy(last.toPath(), dstFile.toPath());
    }

    @Override
    public boolean needSkip(Article article) {
        return false;
    }
}
