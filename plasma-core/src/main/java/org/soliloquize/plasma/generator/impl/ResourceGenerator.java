package org.soliloquize.plasma.generator.impl;

import org.soliloquize.plasma.core.Config;
import org.soliloquize.plasma.generator.Generator;
import org.soliloquize.plasma.model.Article;
import org.soliloquize.plasma.utils.ResourceUtils;

import java.io.File;
import java.util.List;

public class ResourceGenerator implements Generator {

    public static final String[] RESOURCES = new String[] {
            Config.FOLDER_RES
    };

    @Override
    public void generate(List<Article> articleList) throws Exception {
        for (String folder : RESOURCES) {
            File src = new File(Config.getInstance().getSrcFolder(), folder);
            File dst = new File(Config.getInstance().getDstFolder(), folder);
            ResourceUtils.copyFolder(src, dst);
        }
    }

    @Override
    public boolean needSkip(Article article) {
        return false;
    }
}
