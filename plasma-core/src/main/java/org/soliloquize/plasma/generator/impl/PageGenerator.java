package org.soliloquize.plasma.generator.impl;

import com.google.common.collect.Lists;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.soliloquize.plasma.core.Config;
import org.soliloquize.plasma.model.Article;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

@Data
class PageInfo {
    private int page;
    private int total;
    private String baseUrl;

    public PageInfo(int page, int total, String baseUrl) {
        this.page = page;
        this.total = total;
        this.baseUrl = baseUrl;
    }
}

public class PageGenerator extends HtmlGenerator {

    @Override
    protected String getTemplateName() {
        return "page.html";
    }

    @Override
    public void generate(List<Article> articleList) throws Exception {
        generate(articleList, getBaseUrl());
    }

    protected void generate(List<Article> articleList, String baseUrl) throws Exception {
        List<List<Article>> partitions = Lists.partition(articleList, Config.getInstance().getPageSize());
        generatePartitions(partitions, baseUrl);
    }

    protected void generatePartitions(List<List<Article>> partitions, String baseUrl) throws Exception {
        for (int i = 0; i < partitions.size(); i++) {
            int page = i + 1;
            generatePartition(partitions.size(), page, getPageSlug(baseUrl, page), baseUrl, partitions.get(i));
        }
        generatePartition(partitions.size(), 1, getIndexSlug(baseUrl), baseUrl, partitions.get(0));
    }

    protected void generatePartition(int total, int page, String slug, String baseUrl, List<Article> articleList) throws Exception {
        PageInfo pageInfo = new PageInfo(page, total, baseUrl);
        Map<String, Object> context = new HashMap<>();
        context.put("article_list", articleList);
        context.put("page_info", pageInfo);
        context.put("pages", IntStream.range(1, total + 1).toArray());
        save(getOutputFile(slug), render(context));
    }

    protected String getBaseUrl() {
        return "/page/";
    }

    protected String getIndexSlug(String baseUrl) {
        return baseUrl.replaceAll("/page/", StringUtils.EMPTY);
    }

    private String getPageSlug(String baseUrl, int idx) {
        return String.format("%s%d", baseUrl, idx);
    }

    @Override
    public boolean needSkip(Article article) {
        return article.getMeta().isHiddenFromList();
    }
}
