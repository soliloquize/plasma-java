package org.soliloquize.plasma.generator;

import org.soliloquize.plasma.model.Article;

import java.util.List;

public interface Generator {

    void generate(List<Article> articleList) throws Exception;

    boolean needSkip(Article article);

    default boolean isValid() {
        return true;
    }
}
