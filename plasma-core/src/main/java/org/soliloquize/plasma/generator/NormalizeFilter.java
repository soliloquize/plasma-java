package org.soliloquize.plasma.generator;

import com.mitchellbosecke.pebble.error.PebbleException;
import com.mitchellbosecke.pebble.extension.Filter;
import com.mitchellbosecke.pebble.template.EvaluationContext;
import com.mitchellbosecke.pebble.template.PebbleTemplate;
import org.soliloquize.plasma.utils.TextUtils;

import java.util.List;
import java.util.Map;

public class NormalizeFilter implements Filter {

    @Override
    public Object apply(Object input, Map<String, Object> map, PebbleTemplate pebbleTemplate, EvaluationContext evaluationContext, int i) throws PebbleException {
        if (input == null) {
            return null;
        }
        String str = (String) input;
        return TextUtils.normalize(str);
    }

    @Override
    public List<String> getArgumentNames() {
        return null;
    }
}
