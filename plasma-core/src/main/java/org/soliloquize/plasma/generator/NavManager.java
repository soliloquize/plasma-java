package org.soliloquize.plasma.generator;

import com.mitchellbosecke.pebble.template.PebbleTemplate;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.soliloquize.plasma.core.Config;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NavManager {

    @AllArgsConstructor
    @Data
    public static class NavItem {
        private List<String> path;
        private String name;
        private String timestamp;
        private String href;
        private int id;
    }

    private PebbleTemplate templateNavTitle;
    private PebbleTemplate templateNavUl;
    private PebbleTemplate templateNavLi;
    private PebbleTemplate templateNavLink;
    private boolean valid;


    public NavManager() {
        valid = false;
        initTemplates();
    }

    private void initTemplates() {
        try {
            templateNavTitle = GeneratorHelper.getPebbleEngine().getTemplate("nav_title.html");
            templateNavUl = GeneratorHelper.getPebbleEngine().getTemplate("nav_ul.html");
            templateNavLi = GeneratorHelper.getPebbleEngine().getTemplate("nav_li.html");
            templateNavLink = GeneratorHelper.getPebbleEngine().getTemplate("nav_link.html");
            valid = true;
        } catch (Exception e) {
        }
    }

    public boolean isValid() {
        return valid;
    }

    public String generate() throws IOException {
        List<NavItem> itemList = new ArrayList<>();
        String base = getPostPath();
        generateNavItemList(base, itemList);
        List<Object> metaList = generateHtml(itemList);
        return formatHtml(metaList);
    }

    private void generateNavItemList(String base, List<NavItem> itemList) throws IOException {
        List<Path> pathList = Files.list(new File(base).toPath()).collect(Collectors.toList());
        List<Path> folders = new ArrayList<>();
        List<Path> files = new ArrayList<>();
        for (Path path : pathList) {
            if (path.toFile().isDirectory()) {
                folders.add(path);
            } else {
                if (path.toString().endsWith(".md")) {
                    files.add(path);
                }
            }
        }
        Collections.sort(folders);
        for (Path path : folders) {
            generateNavItemList(path.toAbsolutePath().toString(), itemList);
        }
        List<NavItem> tmpList = new ArrayList<>();
        for (Path path : files) {
            List<String> lines = Files.readAllLines(path);
            Map<String, String> meta = parseMeta(lines);
            String name = meta.get("title");
            String timestamp = meta.get("date");
            String href = meta.get("slug");
            List<String> relativePath = getRelativePath(path, new File(getPostPath()).toPath());
            tmpList.add(new NavItem(relativePath, name, timestamp, href, href.hashCode()));

        }
        tmpList.sort((x, y) -> y.timestamp.compareTo(x.timestamp));
        itemList.addAll(tmpList);
    }

    private String getPostPath() {
        return new File(Config.getInstance().getSrcFolder(), Config.FOLDER_POST).getAbsolutePath();
    }

    private Map<String, String> parseMeta(List<String> lines) {
        Map<String, String> meta = new HashMap<>();
        for (String line : lines) {
            if (line.equals("---")) {
                break;
            }
            String[] splits = line.split(":", 2);
            meta.put(splits[0].trim(), splits[1].trim());
        }
        return meta;
    }

    private List<Object> generateHtml(List<NavItem> itemList) {
        List<Object> result = new ArrayList<>();
        for (int i = 0; i < itemList.size(); i++) {
            List<Object> tmpList = result;
            NavItem item = itemList.get(i);
            for (int j = 0; j < item.path.size(); j++) {
                String split = item.path.get(j);
                int idx = indexNav(split, tmpList);
                if (idx == -1) {
                    if (j == item.path.size() - 1) {
                        tmpList.add(item);
                    } else {
                        Map<String, List<Object>> newMap = new HashMap<>();
                        newMap.put(split, new ArrayList<>());
                        tmpList.add(newMap);
                        tmpList = ((Map<String, List<Object>>) tmpList.get(tmpList.size() - 1)).get(split);
                    }
                } else {
                    tmpList = ((Map<String, List<Object>>) tmpList.get(idx)).get(split);
                }
            }
        }
        return result;
    }

    private String formatHtml(List<Object> metaList) throws IOException {
        StringBuilder builder = new StringBuilder();
        for (Object object : metaList) {
            if (object instanceof Map) {
                Map<String, List<Object>> subMap = (Map<String, List<Object>>) object;
                for (Map.Entry<String, List<Object>> entry : subMap.entrySet()) {
                    String content = formatUl(formatHtml(entry.getValue()));
                    builder.append(formatLi(formatTitle(normalizeTitle(entry.getKey())) + content));
                }
            } else {
                NavItem item = (NavItem) object;
                builder.append(formatLi(formatLink(item.name, item.href, item.id)));
            }
        }
        return builder.toString();
    }

    private List<String> getRelativePath(Path path, Path root) {
        String relative =  path.toAbsolutePath().toString().substring(root.toAbsolutePath().toString().length());
        String[] splits = relative.replaceAll("\\\\", "/").split("/");
        List<String> result = Stream.of(splits).collect(Collectors.toList());
        return result.subList(1, result.size());
    }

    private String formatLi(String content) throws IOException {
        return formatByContent(templateNavLi, content);
    }

    private String formatTitle(String content) throws IOException {
        return formatByContent(templateNavTitle, content);
    }

    private String normalizeTitle(String content) {
        String[] splits = content.split("[.]", 2);
        if (splits.length > 1) {
            return splits[1];
        } else {
            return splits[0];
        }
    }

    private String formatUl(String content) throws IOException {
        return formatByContent(templateNavUl, content);
    }

    private String formatLink(String content, String link, int identifier) throws IOException {
        Map<String, Object> context = new HashMap<>();
        context.put("content", content);
        context.put("link", link);
        context.put("identifier", identifier);
        return formatByContext(templateNavLink, context);
    }

    private String formatByContent(PebbleTemplate template, String content) throws IOException {
        Map<String, Object> context = new HashMap<>();
        context.put("content", content);
        return formatByContext(template, context);
    }

    private String formatByContext(PebbleTemplate template, Map<String, Object> context) throws IOException {
        return GeneratorHelper.render(context, template);
    }

    private int indexNav(String item, List<Object> navList) {
        for (int i = 0; i < navList.size(); i++) {
            Object object = navList.get(i);
            if (object instanceof Map && ((Map<String, Object>) object).containsKey(item)) {
                return i;
            }
        }
        return -1;
    }
}
