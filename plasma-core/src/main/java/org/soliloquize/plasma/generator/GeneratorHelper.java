package org.soliloquize.plasma.generator;

import com.mitchellbosecke.pebble.PebbleEngine;
import com.mitchellbosecke.pebble.loader.FileLoader;
import com.mitchellbosecke.pebble.template.PebbleTemplate;
import org.apache.commons.io.IOUtils;
import org.soliloquize.plasma.core.Config;

import java.io.*;
import java.util.Map;

public class GeneratorHelper {

    public static final String OUTPUT_NAME = "index.html";

    public static PebbleEngine getPebbleEngine() {
        FileLoader loader = new FileLoader();
        loader.setPrefix(Config.getInstance().getThemePath());
        return new PebbleEngine.Builder().extension(new CustomExtension()).autoEscaping(false).loader(loader).build();
    }

    public static String render(Map<String, Object> context, PebbleTemplate template) throws IOException {
        Writer writer = new StringWriter();
        context.put("config", Config.getInstance());
        template.evaluate(writer, context);
        return writer.toString();
    }

    public static void save(File file, String content) throws IOException {
        FileWriter writer = new FileWriter(file);
        IOUtils.write(content, writer);
        writer.flush();
        writer.close();
    }
}
