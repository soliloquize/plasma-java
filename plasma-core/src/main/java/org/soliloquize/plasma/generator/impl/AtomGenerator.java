package org.soliloquize.plasma.generator.impl;

import org.soliloquize.plasma.model.Article;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AtomGenerator extends HtmlGenerator {

    public static final int LIMIT = 20;

    @Override
    protected String getTemplateName() {
        return "atom.html";
    }

    @Override
    public void generate(List<Article> articleList) throws Exception {
        Map<String, Object> context = new HashMap<>();
        context.put("article_list", articleList.subList(0, LIMIT));
        save(getOutputFile("atom.xml"), render(context));
    }

    @Override
    public boolean needSkip(Article article) {
        return article.getMeta().isHiddenFromList();
    }
}
