package org.soliloquize.plasma.generator.impl;

import com.mitchellbosecke.pebble.PebbleEngine;
import com.mitchellbosecke.pebble.template.PebbleTemplate;
import org.soliloquize.plasma.core.Config;
import org.soliloquize.plasma.generator.Generator;
import org.soliloquize.plasma.generator.GeneratorHelper;

import java.io.*;
import java.util.Map;

public abstract class HtmlGenerator implements Generator {

    protected ThreadLocal<PebbleEngine> engine = ThreadLocal.withInitial(GeneratorHelper::getPebbleEngine);

    protected ThreadLocal<PebbleTemplate> template = ThreadLocal.withInitial(() -> {
        try {
            return engine.get().getTemplate(getTemplateName());
        } catch (Exception e) {
            return null;
        }
    });

    protected abstract String getTemplateName();

    @Override
    public boolean isValid() {
        return template.get() != null;
    }

    protected File getOutputFile(String subPath) {
        File folder = new File(Config.getInstance().getDstFolder(), subPath);
        folder.mkdirs();
        return new File(folder.getAbsolutePath(), GeneratorHelper.OUTPUT_NAME);
    }

    protected String render(Map<String, Object> context) throws IOException {
        return render(context, template.get());
    }

    protected String render(Map<String, Object> context, String templateName) throws IOException {
        return render(context, engine.get().getTemplate(templateName));
    }

    private String render(Map<String, Object> context, PebbleTemplate template) throws IOException {
        return GeneratorHelper.render(context, template);
    }

    protected void save(File file, String content) throws IOException {
        GeneratorHelper.save(file, content);
    }
}
