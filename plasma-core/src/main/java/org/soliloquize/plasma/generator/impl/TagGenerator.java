package org.soliloquize.plasma.generator.impl;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.soliloquize.plasma.core.Config;
import org.soliloquize.plasma.model.Article;
import org.soliloquize.plasma.utils.TextUtils;

import java.io.File;
import java.util.*;


@Data
class TagInfo {
    private String name;
    private String url;
    private double size;

    public TagInfo(String name, String url, double size) {
        this.name = name;
        this.url = url;
        this.size = size;
    }
}


public class TagGenerator extends PageGenerator {

    public static final String TAGS = "tags";

    public static final String TEMPLATE_TAGS = "tags.html";

    @Override
    public boolean isValid() {
        try {
            engine.get().getTemplate(TEMPLATE_TAGS);
            return super.isValid();
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void generate(List<Article> articleList) throws Exception {
        Map<String, List<Article>> tags = groupByTag(articleList);
        Map<String, String> tagPaths = createTagPaths(tags.keySet());
        for (Map.Entry<String, List<Article>> entry : tags.entrySet()) {
            super.generate(entry.getValue(), tagPaths.get(entry.getKey()));
        }
        generateEntrance(tags, tagPaths);
    }

    private void generateEntrance(Map<String, List<Article>> tags, Map<String, String> tagPaths) throws Exception {
        List<TagInfo> tagInfoList = buildTagInfoList(tags, tagPaths);
        Map<String, Object> context = new HashMap<>();
        context.put("tag_list", tagInfoList);
        Collections.shuffle(tagInfoList);
        save(getOutputFile(TAGS), render(context, TEMPLATE_TAGS));
    }

    private List<TagInfo> buildTagInfoList(Map<String, List<Article>> tags, Map<String, String> tagPaths) {
        List<TagInfo> tagInfoList = new ArrayList<>();
        int total = tags.entrySet().stream().mapToInt(x -> x.getValue().size()).sum();
        double avg = total * 3.0 / tags.size();
        for (Map.Entry<String, List<Article>> entry : tags.entrySet()) {
            String tag = entry.getKey();
            double size = Math.min(8 + (1.0 * entry.getValue().size() / avg) * 15, 40);
            tagInfoList.add(new TagInfo(tag, tagPaths.get(tag), size));
        }
        return tagInfoList;
    }

    private Map<String, List<Article>> groupByTag(List<Article> articleList) {
        Map<String, List<Article>> tags = new HashMap<>();
        for (Article article : articleList) {
            for (String tag : article.getMeta().getTags()) {
                if (StringUtils.isEmpty(tag.trim())) {
                    continue;
                }
                if (!tags.containsKey(tag)) {
                    tags.put(tag, new ArrayList<>());
                }
                tags.get(tag).add(article);
            }
        }
        return tags;
    }

    private Map<String, String> createTagPaths(Collection<String> tags) {
        Map<String, String> tagPaths = new HashMap<>();
        for (String tag : tags) {
            String path = String.format("/%s/%s/", TAGS, TextUtils.normalize(tag));
            File file = new File(Config.getInstance().getDstFolder(), path);
            file.mkdirs();
            tagPaths.put(tag, path);
        }
        return tagPaths;
    }

    @Override
    public boolean needSkip(Article article) {
        return false;
    }
}
