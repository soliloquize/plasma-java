package org.soliloquize.plasma.generator.impl;

import org.apache.commons.io.IOUtils;
import org.soliloquize.plasma.core.Config;
import org.soliloquize.plasma.generator.Generator;
import org.soliloquize.plasma.model.Article;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

public class StaticGenerator implements Generator {

    @Override
    public void generate(List<Article> articleList) throws Exception {
        String staticPath = Config.getInstance().getThemePath() + "static/";
        String dst = Config.getInstance().getDstFolder();

        Stream<Path> pathList= Files.walk(new File(staticPath).toPath());
        pathList.forEach(x -> {
            try {
                if (x.toFile().isDirectory()) {
                    return;
                }
                String path = x.toAbsolutePath().toString();
                int idx = path.indexOf("static");
                File dstFile = new File(dst, path.substring(idx));
                dstFile.getParentFile().mkdirs();
                byte[] bytes = IOUtils.toByteArray(new FileInputStream(new File(path)));
                IOUtils.write(bytes, new FileOutputStream(dstFile));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public boolean needSkip(Article article) {
        return false;
    }
}
