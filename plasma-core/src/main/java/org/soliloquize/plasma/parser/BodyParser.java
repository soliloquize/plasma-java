package org.soliloquize.plasma.parser;

public interface BodyParser {

    String parse(String content);
}
