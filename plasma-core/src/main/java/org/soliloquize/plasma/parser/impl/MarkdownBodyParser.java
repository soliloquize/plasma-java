package org.soliloquize.plasma.parser.impl;


import com.vladsch.flexmark.Extension;
import com.vladsch.flexmark.ast.Node;
import com.vladsch.flexmark.ext.emoji.EmojiExtension;
import com.vladsch.flexmark.ext.emoji.EmojiImageType;
import com.vladsch.flexmark.ext.emoji.EmojiShortcutType;
import com.vladsch.flexmark.ext.gfm.strikethrough.StrikethroughExtension;
import com.vladsch.flexmark.ext.tables.TablesExtension;
import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.options.MutableDataSet;
import org.soliloquize.plasma.parser.BodyParser;

import java.util.Arrays;

public class MarkdownBodyParser implements BodyParser {

    private Parser parser;
    private MutableDataSet options;

    public MarkdownBodyParser() {
        options = new MutableDataSet();
        Extension[] extensions = new Extension[] {
                EmojiExtension.create(),
                StrikethroughExtension.create(),
                TablesExtension.create(),
        };
        options.set(Parser.EXTENSIONS, Arrays.asList(extensions));
        options.set(EmojiExtension.USE_SHORTCUT_TYPE, EmojiShortcutType.GITHUB);
        options.set(EmojiExtension.USE_IMAGE_TYPE, EmojiImageType.IMAGE_ONLY);
        parser = Parser.builder(options).build();
    }

    public String parse(String content) {
        Node document = parser.parse(content);
        HtmlRenderer renderer = HtmlRenderer.builder(options).build();
        return renderer.render(document);
    }
}
