package org.soliloquize.plasma.parser.impl;

import org.apache.commons.lang3.StringUtils;
import org.soliloquize.plasma.model.Meta;
import org.soliloquize.plasma.parser.MetaParser;
import org.soliloquize.plasma.utils.TextUtils;

import java.util.HashMap;
import java.util.Map;

public class PropertyMetaParser implements MetaParser {

    public static final String SEP = ":";
    public static final String[] ESCAPE_ITEMS = new String[]{"\\[", "\\]"};
    public static final int SPLIT_LIMIT = 2;

    public Meta parse(String content) {
        String[] lines = content.split("\n");
        Map<String, String> properties = new HashMap<>();
        for (String line : lines) {
            String[] splits = line.split(SEP, SPLIT_LIMIT);
            String key = splits[0].trim();
            if (splits.length > 1) {
                properties.put(key, TextUtils.escape(splits[1].trim(), ESCAPE_ITEMS));
            } else {
                properties.put(key, StringUtils.EMPTY);
            }
        }
        return Meta.createFromMap(properties);
    }
}
