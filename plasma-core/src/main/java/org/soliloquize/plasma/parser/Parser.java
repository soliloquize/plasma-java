package org.soliloquize.plasma.parser;

import org.apache.commons.lang3.tuple.Pair;
import org.soliloquize.plasma.model.Article;
import org.soliloquize.plasma.model.Meta;
import org.soliloquize.plasma.parser.impl.LineSplitter;
import org.soliloquize.plasma.parser.impl.MarkdownBodyParser;
import org.soliloquize.plasma.parser.impl.PropertyMetaParser;

public class Parser {

    private MetaParser metaParser;
    private BodyParser bodyParser;
    private Splitter splitter;

    public Parser() {
        metaParser = new PropertyMetaParser();
        bodyParser = new MarkdownBodyParser();
        splitter = new LineSplitter();
    }

    public Article parse(String content) {
        Pair<String, String> pair = splitter.split(content);
        Meta meta = metaParser.parse(pair.getLeft());
        String body = bodyParser.parse(pair.getRight());
        Article article = new Article();
        article.setBody(body);
        article.setMeta(meta);
        return article;
    }
}
