package org.soliloquize.plasma.parser.impl;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.soliloquize.plasma.parser.Splitter;

public class LineSplitter implements Splitter {

    public static final String SEP = "---";

    public Pair<String, String> split(String content) {
        int idx = content.indexOf(SEP);
        String left = content.substring(0, idx);
        String right = content.substring(idx + SEP.length());
        return new ImmutablePair<>(left, right);
    }
}
