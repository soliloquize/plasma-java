package org.soliloquize.plasma.parser;

import org.apache.commons.lang3.tuple.Pair;

public interface Splitter {

    Pair<String, String> split(String content);
}
