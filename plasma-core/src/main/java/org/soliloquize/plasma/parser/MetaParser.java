package org.soliloquize.plasma.parser;

import org.soliloquize.plasma.model.Meta;

public interface MetaParser {
    Meta parse(String content);
}
