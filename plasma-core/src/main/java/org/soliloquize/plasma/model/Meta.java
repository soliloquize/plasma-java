package org.soliloquize.plasma.model;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
public class Meta {
    public static final String TITLE = "title";
    public static final String TAGS = "tags";
    public static final String CREATED_TIME = "date";
    public static final String SLUG = "slug";
    public static final String HIDDEN_FROM_LIST = "not_in_list";
    public static final String SEP = ",";

    private String title;
    private List<String> tags;
    private String createdTime;
    private String slug;
    private boolean isHiddenFromList;
    private String series;
    private int uniqueId;

    public static Meta createFromMap(Map<String, String> properties) {
        Meta meta = new Meta();
        meta.setTitle(properties.get(TITLE));
        String[] tags = properties.getOrDefault(TAGS, StringUtils.EMPTY).split(SEP);
        meta.setTags(Arrays.stream(tags).map(String::trim).collect(Collectors.toList()));
        meta.setCreatedTime(properties.get(CREATED_TIME));
        meta.setSlug(properties.get(SLUG));
        meta.setHiddenFromList(!StringUtils.isEmpty(properties.getOrDefault(HIDDEN_FROM_LIST, StringUtils.EMPTY)));
        meta.setUniqueId(properties.getOrDefault(SLUG, "").hashCode());
        return meta;
    }
}
