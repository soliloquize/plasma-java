package org.soliloquize.plasma.model;

import lombok.Data;

@Data
public class Article implements Comparable<Article> {

    private Meta meta;
    private String body;

    @Override
    public int compareTo(Article a) {
        return meta.getCreatedTime().compareTo(a.getMeta().getCreatedTime());
    }
}
