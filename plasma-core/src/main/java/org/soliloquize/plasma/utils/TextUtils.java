package org.soliloquize.plasma.utils;

import org.apache.commons.lang3.StringUtils;

public class TextUtils {

    public static String escape(String content, String[] items) {
        if (StringUtils.isEmpty(content)) {
            return StringUtils.EMPTY;
        }
        for (String item : items) {
            content = content.replaceAll(item, StringUtils.EMPTY);
        }
        return content;
    }

    public static String normalize(String content) {
        content = content.replaceAll("\\p{Punct}", "");
        content = content.replaceAll("\\pP", "");
        content = content.replaceAll(" +", "__");
        content = content.replaceAll("__", "-");
        content = content.replaceAll("-+", "-");
        content = content.replace(".", "");
        content = content.replace("\"", "");
        content = content.replace("'", "");
        return content;
    }
}
