package org.soliloquize.plasma.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

public class ResourceUtils {

    public static void copyFolder(File src, File dst) throws IOException {
        Files.walkFileTree(src.toPath(), new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                String relative = file.toAbsolutePath().toString().replace(src.getAbsolutePath(), "");
                File dstFile = new File(dst, relative);
                dstFile.getParentFile().mkdirs();
                Files.copy(file, dstFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                return super.visitFile(file, attrs);
            }
        });
    }
}
