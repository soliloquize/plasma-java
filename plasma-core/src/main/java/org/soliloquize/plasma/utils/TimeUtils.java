package org.soliloquize.plasma.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TimeUtils {

    public static String FORMAT_NORMAL = "yyyy-MM-dd HH:mm:ss";
    public static String FORMAT_URL = "/yyyy/MM/dd";

    public static String reformatDate(String content, String fmt1, String fmt2) {
        LocalDateTime dt = LocalDateTime.parse(content, DateTimeFormatter.ofPattern(fmt1));
        return dt.format(DateTimeFormatter.ofPattern(fmt2));
    }

    public static String now(String fmt) {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(fmt));
    }
}
