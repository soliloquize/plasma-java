package org.soliloquize.plasma.core;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.soliloquize.plasma.generator.impl.*;
import org.soliloquize.plasma.model.Article;
import org.soliloquize.plasma.parser.Parser;
import org.soliloquize.plasma.generator.Generator;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Plasma {

    public static final String EXT_MARKDOWN = ".md";
    private List<Generator> postGeneratorList;
    private List<Generator> pageGeneratorList;

    public Plasma() {
        postGeneratorList = Arrays.asList(
                new ArticleGenerator(true),
                new AtomGenerator(),
                new PageGenerator(),
                new TagGenerator(),
                new ResourceGenerator(),
                new StaticGenerator()
        );
        pageGeneratorList = Arrays.asList(
                new ArticleGenerator(false)
        );
    }

    public void build() throws Exception {
        File srcFolder = new File(Config.getInstance().getSrcFolder());
        File dstFolder = new File(Config.getInstance().getDstFolder());
        ensureDirectory(srcFolder, dstFolder);
        File postFolder = new File(srcFolder, Config.FOLDER_POST);
        handleGenerators(postGeneratorList, getArticleList(listAllMarkdownFiles(postFolder)));
        File pageFolder = new File(srcFolder, Config.FOLDER_PAGE);
        handleGenerators(pageGeneratorList, getArticleList(listAllMarkdownFiles(pageFolder)));
    }

    private void handleGenerators(List<Generator> generatorList, List<Article> articleList) throws Exception {
        for (Generator generator : generatorList) {
            List<Article> candidateList = articleList.stream().filter(x -> !generator.needSkip(x)).collect(Collectors.toList());
            if (!generator.isValid()) {
                continue;
            }
            generator.generate(candidateList);
        }
    }

    private List<Article> getArticleList(List<Path> pathList) throws Exception {
        List<Article> articleList = new ArrayList<>();
        Parser parser = new Parser();
        for (Path path : pathList) {
            String content = IOUtils.toString(path.toUri(), Charset.forName("UTF-8"));
            articleList.add(parser.parse(content));
        }
        Collections.sort(articleList);
        Collections.reverse(articleList);
        return articleList;
    }

    private void ensureDirectory(File srcFolder, File dstFolder) throws IOException {
        assert srcFolder.exists();
        if (!dstFolder.exists()) {
            dstFolder.mkdirs();
        }
        for (File file : dstFolder.listFiles()) {
            if (file.isDirectory()) {
                FileUtils.deleteDirectory(file);
            } else {
                file.delete();
            }
        }
    }

    private List<Path> listAllMarkdownFiles(File srcFolder) {
        try {
            List<Path> result = new ArrayList<>();
            Files.walkFileTree(srcFolder.toPath(), new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    String path = file.toString();
                    if (path.endsWith(EXT_MARKDOWN)) {
                        result.add(file);
                    }
                    return super.visitFile(file, attrs);
                }
            });
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }
}
