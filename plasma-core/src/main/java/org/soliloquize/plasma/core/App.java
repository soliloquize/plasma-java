package org.soliloquize.plasma.core;

import com.mitchellbosecke.pebble.PebbleEngine;
import com.mitchellbosecke.pebble.loader.ClasspathLoader;
import com.mitchellbosecke.pebble.template.PebbleTemplate;
import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.soliloquize.plasma.generator.GeneratorHelper;
import org.soliloquize.plasma.utils.TextUtils;
import org.soliloquize.plasma.utils.TimeUtils;

import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


public class App {

    public static final String COMMAND_BUILD = "build";
    public static final String COMMAND_CREATE = "create";
    public static final String COMMAND_INIT = "init";

    public static void main(String[] args) throws Exception {
        CommandLine commandLine = handleArgs(args);
        if (commandLine.hasOption("c")) {
            String path = commandLine.getOptionValue("c");
            initConfig(path);
        }
        String command = commandLine.getOptionValue("m");
        switch (command) {
            case COMMAND_INIT:
                init();
                break;
            case COMMAND_BUILD:
                build();
                break;
            case COMMAND_CREATE:
                create(commandLine.getArgs()[0]);
                break;
        }
    }

    private static void init() throws Exception {
        File[] folders = new File[] {
                new File(Config.FOLDER_SOURCES),
                new File(Config.FOLDER_SOURCES, Config.FOLDER_POST),
                new File(Config.FOLDER_SOURCES, Config.FOLDER_DRAFT),
                new File(Config.FOLDER_SOURCES, Config.FOLDER_PAGE),
                new File(Config.FOLDER_SOURCES, Config.FOLDER_RES),
                new File(Config.FOLDER_BIN),
                new File(Config.FOLDER_OUTPUT),
        };
        for (File folder : folders) {
            folder.mkdirs();
        }
        String content = IOUtils.resourceToString("/fabfile.py", Charset.forName("utf-8"));
        GeneratorHelper.save(new File("fabfile.py"), content);
        content = IOUtils.resourceToString("/config.properties", Charset.forName("utf-8"));
        GeneratorHelper.save(new File(Config.FOLDER_BIN, "config.properties"), content);
        for (File jar : FileUtils.listFiles(new File("."), new String[] {".jar"}, false)) {
            if (jar.getName().startsWith("plasma-java")) {
                FileUtils.copyFile(jar, new File(Config.FOLDER_BIN, jar.getName()));
            }
        }
    }

    private static void build() throws Exception {
        long start = Instant.now().toEpochMilli();
        Plasma plasma = new Plasma();
        plasma.build();
        long end = Instant.now().toEpochMilli();
        System.out.println("cost: " + (end - start));
    }

    private static void create(String title) throws Exception {
        long start = Instant.now().toEpochMilli();
        ClasspathLoader loader = new ClasspathLoader();
        PebbleEngine engine = new PebbleEngine.Builder().loader(loader).build();
        PebbleTemplate template = engine.getTemplate("article.md");
        Map<String, Object> context = new HashMap<>();
        context.put("title", title);
        context.put("now", TimeUtils.now(TimeUtils.FORMAT_NORMAL));
        context.put("slug", TimeUtils.now(TimeUtils.FORMAT_URL) + "/" + TextUtils.normalize(title));
        context.put("tags", "");
        context.put("not_in_list", "");
        Config config = Config.getInstance();
        File out = new File(config.getSrcFolder(), TextUtils.normalize(title) + ".md");
        GeneratorHelper.save(out, GeneratorHelper.render(context, template));
        long end = Instant.now().toEpochMilli();
        System.out.println("cost: " + (end - start));
    }

    private static CommandLine handleArgs(String[] args) throws Exception {
        Options options = new Options();
        Option opt1 = new Option("c", "config path", true, "config path");
        opt1.setRequired(false);
        options.addOption(opt1);
        Option opt2 = new Option("m", "command", true, "command");
        opt2.setRequired(true);
        options.addOption(opt2);
        CommandLineParser parser = new DefaultParser();
        return parser.parse(options, args);
    }

    private static void initConfig(String path) throws Exception {
        Properties properties = new Properties();
        properties.load(new FileInputStream(new File(path)));
        Config config = Config.getInstance();
        config.setSiteName(properties.getProperty("site_name"));
        config.setSiteUrl(properties.getProperty("site_url"));
        config.setAuthor(properties.getProperty("author"));
        config.setThemePath(properties.getProperty("theme"));
        config.setPageSize(Integer.parseInt(properties.getProperty("page_size")));
        config.setBaidu(properties.getProperty("baidu", null));
        config.setStats(properties.getProperty("stats", null));

        String src = properties.getProperty("src", null);
        String dst = properties.getProperty("dst", null);
        if (src == null) {
            src = new File(Config.FOLDER_SOURCES).getAbsolutePath();
        }
        if (dst == null) {
            dst = new File(Config.FOLDER_OUTPUT).getAbsolutePath();
        }
        config.setSrcFolder(src);
        config.setDstFolder(dst);
        System.out.println("src: " + src);
        System.out.println("dst: " + dst);
    }
}
