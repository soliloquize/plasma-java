package org.soliloquize.plasma.core;

import lombok.Data;

@Data
public class Config {

    public static final String FOLDER_BIN = "bin";
    public static final String FOLDER_RES = "resources";
    public static final String FOLDER_SOURCES = "sources";
    public static final String FOLDER_POST = "posts";
    public static final String FOLDER_DRAFT = "drafts";
    public static final String FOLDER_PAGE = "pages";
    public static final String FOLDER_OUTPUT = "public";
    public static final String FOLDER_IMAGE = "public_image";
    public static final String FOLDER_FILE = "public_file";

    private static volatile Config instance;

    public static Config getInstance() {
        if (instance == null) {
            synchronized (Config.class) {
                instance = new Config();
            }
        }
        return instance;
    }

    private String siteName;
    private String siteUrl;
    private String srcFolder;
    private String dstFolder;
    private String themePath;
    private String author;
    private String baidu;
    private String stats;
    private int pageSize;
}
