package org.soliloquize.plasma;

import org.junit.Assert;
import org.junit.Test;
import org.soliloquize.plasma.core.Config;
import org.soliloquize.plasma.generator.NavManager;

import java.io.File;
import java.nio.file.Path;

public class NavManagerTest {

    @Test
    public void testGenerate() throws Exception {
        Path current =  new File("").toPath().toAbsolutePath();
        String srcPath = current.toString() + "/src/test/resources/nav";
        String themePath = current.getParent().toString() + "/plasma-theme/theme-book/";
        Config config = Config.getInstance();
        config.setSrcFolder(srcPath);
        config.setThemePath(themePath);

        NavManager manager = new NavManager();
        String content = manager.generate();
        System.out.println(content);
        Assert.assertTrue(content.length() > 0);
    }
}
