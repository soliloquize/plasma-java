package org.soliloquize.plasma.parser;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.soliloquize.plasma.model.Article;

import java.nio.charset.Charset;

public class ParserTest {

    @Test
    public void testParse() throws Exception {
        String content = IOUtils.resourceToString("/article.txt", Charset.forName("utf-8"));
        Parser parser = new Parser();
        Article article = parser.parse(content);
        Assert.assertEquals("plasma-java", article.getMeta().getTitle());
        Assert.assertNotNull(article.getBody());
    }
}
