package org.soliloquize.plasma.parser.impl;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Test;
import org.soliloquize.plasma.parser.Splitter;

import java.nio.charset.Charset;

public class LineSplitterTest {

    @Test
    public void testSplit() throws Exception {
        String content = IOUtils.resourceToString("/splitter.txt", Charset.forName("utf-8"));
        Splitter splitter = new LineSplitter();
        Pair<String, String> pair = splitter.split(content);
        Assert.assertEquals("top", pair.getLeft().trim());
        Assert.assertEquals("bottom", pair.getRight().trim());
    }
}
