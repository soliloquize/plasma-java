package org.soliloquize.plasma.parser.impl;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.soliloquize.plasma.parser.BodyParser;

import java.nio.charset.Charset;

public class MarkdownBodyParserTest {

    @Test
    public void testParse() throws Exception {
        String content = IOUtils.resourceToString("/body.txt", Charset.forName("utf-8"));
        BodyParser parser = new MarkdownBodyParser();
        String html = parser.parse(content);
        System.out.println(html);
    }
}
