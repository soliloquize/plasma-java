package org.soliloquize.plasma.parser.impl;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.soliloquize.plasma.model.Meta;
import org.soliloquize.plasma.parser.MetaParser;

import java.nio.charset.Charset;

public class PropertyMetaParserTest {

    @Test
    public void testParse() throws Exception {
        String content = IOUtils.resourceToString("/meta.txt", Charset.forName("utf-8"));
        MetaParser parser = new PropertyMetaParser();
        Meta meta = parser.parse(content);
        Assert.assertEquals("plasma-java", meta.getTitle());
        Assert.assertEquals("2018-07-21 10:30:00", meta.getCreatedTime());
        Assert.assertFalse(meta.isHiddenFromList());
        Assert.assertEquals("Java", meta.getTags().get(0));
        Assert.assertEquals("Blog", meta.getTags().get(1));
    }
}
