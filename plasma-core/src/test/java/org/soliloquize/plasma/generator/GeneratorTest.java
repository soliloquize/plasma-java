package org.soliloquize.plasma.generator;

import com.mitchellbosecke.pebble.PebbleEngine;
import com.mitchellbosecke.pebble.loader.ClasspathLoader;
import com.mitchellbosecke.pebble.template.PebbleTemplate;
import org.junit.Test;

import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

public class GeneratorTest {

    @Test
    public void testGenerate() throws Exception {
        PebbleEngine engine = new PebbleEngine.Builder().loader(new ClasspathLoader()).build();
        Writer writer = new StringWriter();
        PebbleTemplate compiledTemplate = engine.getTemplate("home.html");
        Map<String, Object> context = new HashMap<>();
        context.put("websiteTitle", "My First Website");
        context.put("content", "My Interesting Content");
        compiledTemplate.evaluate(writer, context);
        String content = writer.toString();
        System.out.println(content);
    }
}
