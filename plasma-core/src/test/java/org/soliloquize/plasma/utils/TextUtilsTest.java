package org.soliloquize.plasma.utils;

import org.junit.Assert;
import org.junit.Test;
import org.soliloquize.plasma.parser.impl.PropertyMetaParser;

public class TextUtilsTest {

    @Test
    public void testEscape() {
        String content = "[北京, 杭州]";
        String result = TextUtils.escape(content, PropertyMetaParser.ESCAPE_ITEMS);
        Assert.assertEquals("北京, 杭州", result);
    }

    @Test
    public void testNormalize() {
        String content = "Android Studio(一)（）：怎么使用";
        String result = TextUtils.normalize(content);
        Assert.assertEquals("Android-Studio一怎么使用", result);
    }
}
