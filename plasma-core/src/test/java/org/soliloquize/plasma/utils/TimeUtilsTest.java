package org.soliloquize.plasma.utils;

import org.junit.Assert;
import org.junit.Test;

public class TimeUtilsTest {

    @Test
    public void testReformatDate() {
        String time = "2018-07-07 08:23:45";
        String result = TimeUtils.reformatDate(time, TimeUtils.FORMAT_NORMAL, TimeUtils.FORMAT_URL);
        Assert.assertEquals("/2018/07/07", result);
    }
}
